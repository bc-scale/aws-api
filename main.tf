# retrieve current region
data "aws_region" "cr" {}

# retrieve current identity
data "aws_caller_identity" "ci" {}

# retrieve api http
data "aws_apigatewayv2_api" "ApiHttp" {
  api_id = var.api_http_id
}

# define lambda integration
resource "aws_apigatewayv2_integration" "ApiHttpIntegration" {
  api_id                 = var.api_http_id
  integration_type       = "AWS_PROXY"
  integration_method     = "POST"
  integration_uri        = "arn:aws:apigateway:${data.aws_region.cr.name}:lambda:path/2015-03-31/functions/arn:aws:lambda:${data.aws_region.cr.name}:${data.aws_caller_identity.ci.account_id}:function:${var.lambda_name}:$${stageVariables.env}/invocations"
  payload_format_version = "2.0"
  response_parameters {
    status_code = 200
    mappings    = {
      "append:header.Cache-Control" = "no-cache, no-store, must-revalidate"
      "append:header.Pragma"        = "no-cache"
      "append:header.Expires"       = "0"
    }
  }
}

# define api route
resource "aws_apigatewayv2_route" "ApiHttpRouteWithJWT" {
  api_id             = var.api_http_id
  route_key          = "${var.method} ${var.route}"
  target             = "integrations/${aws_apigatewayv2_integration.ApiHttpIntegration.id}"
  authorization_type = "JWT"
  authorizer_id      = var.authorizer_id
  count              = (var.authorizer_id == "" ? 0 : 1)
}
resource "aws_apigatewayv2_route" "ApiHttpRouteWithoutJWT" {
  api_id             = var.api_http_id
  route_key          = "${var.method} ${var.route}"
  target             = "integrations/${aws_apigatewayv2_integration.ApiHttpIntegration.id}"
  count              = (var.authorizer_id == "" ? 1 : 0)
}

# define lambda permissions
resource "aws_lambda_permission" "ApiHttpPermissionDev" {
  action        = "lambda:InvokeFunction"
  function_name = var.lambda_name
  qualifier     = var.lambda_dev
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${data.aws_region.cr.name}:${data.aws_caller_identity.ci.account_id}:${var.api_http_id}/${var.lambda_dev}/${var.method}${var.route}"
}
resource "aws_lambda_permission" "ApiHttpPermissionProd" {
  action        = "lambda:InvokeFunction"
  function_name = var.lambda_name
  qualifier     = var.lambda_prod
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${data.aws_region.cr.name}:${data.aws_caller_identity.ci.account_id}:${var.api_http_id}/${var.lambda_prod}/${var.method}${var.route}"
}
