output "endpoint_dev" {
  value       = "${data.aws_apigatewayv2_api.ApiHttp.api_endpoint}/${var.lambda_dev}${var.route}"
  description = "Api DEV url"
}

output "endpoint_prod" {
  value       = "${data.aws_apigatewayv2_api.ApiHttp.api_endpoint}/${var.lambda_prod}${var.route}"
  description = "Api PROD url"
}
